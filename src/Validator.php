<?php namespace SarojPant\Validation;

/**
 * Class Validator
 *
 * @author Saroj Pant
 */
class Validator
{
    // Data to validate
    protected $data = [];

    // Rules for fields
    protected $rules = [];

    // Custom error messages for rules
    protected $customMessages = [];

    // Error messages
    protected $errors = [];

    /**
     * Validates data against rules
     *
     * @param  array $data          data
     * @param  array $rules         rules
     * @param  array $errorMessages error messages
     * @throws \Exception
     *
     * @return $this
     */
    public function validate(array $data, array $rules, array $errorMessages = [])
    {
        // Throw exception if the rule fields are not present in data fields
        if (empty($rules) || !array_intersect_key($rules, $data)) {
            throw new \Exception(
                "Please check provided rules. They cannot be empty and must have rule for at "
                . "least one field."
            );
        }

        $this->data = $data;
        $this->rules = $rules;
        $this->customMessages = $errorMessages;

        // Loop through and validate
        foreach ($rules as $_field => $_rules) {
            // Rule format = required|min:6|between:5:6';
            // Pipe (|) separates rules.
            // argument(s) are separated by colon (:)
            $ruleNames = explode('|', $_rules);

            foreach ($ruleNames as $_ruleName) {
                // explode arguments
                $parts = explode(':', $_ruleName);

                // Get the rule method and remove from array
                // All the methods are prefixed 'validate' to avoid naming issues
                // Method names are camelCased for psr 4 compliance.
                $ruleMethod = 'validate' . ucfirst(array_shift($parts));
                if (!method_exists($this, $ruleMethod)) {
                    throw new \Exception("$_ruleName rule is not valid");
                }

                // Add field name as first argument
                array_unshift($parts, $_field);
                $argumentCount = count($parts);

                // Check if arguments match
                $reflectionMethod = new \ReflectionMethod(get_class($this), $ruleMethod);

                // Get required parameter. field is always available so discount it
                $requiredParameters = $reflectionMethod->getNumberOfRequiredParameters() - 1;

                if ($requiredParameters > $argumentCount) {
                    throw new \Exception("$_ruleName rule requires at least " . $requiredParameters . ' arguments');
                }

                // Run the method
                call_user_func_array(array($this, $ruleMethod), $parts);
            }
        }

        return $this;
    }


    /**
     * Validates a required field
     * Usage: ['title' => 'required']
     *
     * @param $field
     */
    protected function validateRequired($field)
    {
        if (!isset($this->data[$field]) || !trim($this->data[$field])) {
            $this->setError($field, 'required', ucfirst($field) . " is required.");
        }
    }

    /**
     * Validates that the field value matches given string
     * Usage: ['email' => 'equals:something']
     *
     * @param $field
     * @param $value
     */
    protected function validateEquals($field, $value)
    {
        if (!isset($this->data[$field]) || $this->data[$field] != $value) {
            $this->setError($field, 'equals', ucfirst($field) . " does not match {$value}");
        }
    }

    /**
     * Validates if the field value is boolean true.
     * Usage ['booleanField' => 'equalsBooleanTrue']
     *
     * @param $field
     */
    protected function validateEqualsBooleanTrue($field)
    {
        if (!(isset($this->data[$field]) && is_bool($this->data[$field]) && $this->data[$field] === true)) {
            $this->setError($field, 'equalsBooleanTrue');
        }
    }

    /**
     * Validates if the field value is boolean false
     * Usage ['booleanField' => 'equalsBooleanFalse']
     *
     * @param $field
     */
    protected function validateEqualsBooleanFalse($field)
    {
        if (!(isset($this->data[$field]) && is_bool($this->data[$field]) && $this->data[$field] === false)) {
            $this->setError($field, 'equalsBooleanFalse');
        }
    }

    /**
     * Validates an email address
     * Usage: ['txtEmail' => 'email']
     *
     * @param $field
     */
    protected function validateEmail($field)
    {
        if (!filter_var($this->data[$field], FILTER_VALIDATE_EMAIL)) {
            $this->setError($field, 'email');
        }
    }

    /**
     * Validates integer
     * Usage: ['amount' => 'int']
     *
     * @param $field
     */
    protected function validateInt($field)
    {
        if (filter_var($this->data[$field], FILTER_VALIDATE_INT) === false) {
            $this->setError($field, 'int');
        }
    }

    /**
     * Validates unsigned number
     * Usage: ['amount' => 'unsigned']
     *
     * @param $field
     */
    protected function validateUnsigned($field)
    {
        if (!is_numeric($this->data[$field]) || $this->data[$field] < 0) {
            $this->setError($field, 'unsigned');
        }
    }

    /**
     * Validates minimum value
     * Usage: ['minimum_cart_total' => 'min:30']
     * @param $field
     * @param $value
     */
    protected function validateMin($field, $value)
    {
        if (!is_numeric($this->data[$field]) || (float) $this->data[$field] < (float) $value) {
            $this->setError($field, 'min');
        }
    }

    /**
     * Validates maximum number
     * Usage: ['maximum_age' => 'max:50']
     *
     * @param $field
     * @param $value
     */
    protected function validateMax($field, $value)
    {
        if (!is_numeric($this->data[$field]) || (float) $this->data[$field] > (float) $value) {
            $this->setError($field, 'max');
        }
    }

    /**
     * Validates string min length
     *
     * @param $field
     * @param $length
     */
    protected function validateMinLength($field, $length)
    {
        if (strlen($this->data[$field]) < $length) {
            $this->setError($field, 'minLength', ucfirst($field) . " must be at least $length characters long.");
        }
    }

    /**
     * Validates string max length
     *
     * @param $field
     * @param $length
     */
    protected function validateMaxLength($field, $length)
    {
        if (strlen($this->data[$field]) > $length) {
            $this->setError($field, 'maxLength', ucfirst($field) . " must be less than $length characters long.");
        }
    }

    /**
     * Validates that a value exists in a list in csv format
     * Usage: ['name' => 'inCsv:peter,samantha,jared']
     * @param $field
     * @param $csvValues
     */
    protected function validateInCsv($field, $csvValues)
    {
        if (!in_array($this->data[$field], explode(',', $csvValues))) {
            $this->setError($field, 'csv_in');
        }
    }

    /**
     * Sets error message for the given field and rule name
     *
     * @param string $field field name
     * @param string $ruleName rule name
     * @param string $defaultMessage default message
     * @return string
     */
    protected function setError($field, $ruleName, $defaultMessage = '')
    {
        if (isset($this->customMessages[$field . '.' . $ruleName])) {
            return $this->errors[$field][$ruleName] = $this->customMessages[$field . '.' . $ruleName];
        } elseif (isset($this->customMessages[$field])) {
            return $this->errors[$field][$ruleName] = $this->customMessages[$field];
        }

        return $this->errors[$field][$ruleName] = $defaultMessage ?: ucfirst($field)
            . ' is not a valid ' . $ruleName;
    }

    /**
     * Returns validation result - true if passed, false otherwise.
     *
     * @return bool
     */
    public function passed()
    {
        return empty($this->errors);
    }

    /**
     * Returns validation result - true if failed, pass otherwise.
     *
     * @return bool
     */
    public function failed()
    {
        return !$this->passed();
    }

    /**
     * Returns validation error messages.
     *
     * @param null|string $field
     *
     * @return array
     */
    public function getErrors($field = null)
    {
        if (!$field) {
            return $this->errors;
        }

        return isset($this->errors[$field]) ? $this->errors[$field] : [];
    }

    /**
     * Get first error
     *
     * @param null|string $field
     *
     * @return string
     */
    public function getFirstError($field = null)
    {
        $errors = $this->getErrors($field);

        return !empty($errors) ? reset($errors) : '';
    }

    /**
     * Return if there is error
     *
     * @param null|string $field
     *
     * @return bool
     */
    public function hasError($field = null)
    {
        return $this->getFirstError($field) ? true : false;
    }

    /**
     * Resets data, rules, error and validation messages
     *
     * @return $this
     */
    public function reset()
    {
        $this->data = [];
        $this->rules = [];
        $this->customMessages = [];
        $this->errors = [];

        return $this;
    }
}
