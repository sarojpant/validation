<?php

use SarojPant\Validation\Validator;

class ValidatorTest extends \PHPUnit_Framework_TestCase
{
    /** @var  Validator */
    protected $validator;

    public function setUp()
    {
        parent::setUp();

        $this->validator = new Validator();
    }

    public function testValidateRequired()
    {
        $data = ['name' => 'Sp'];
        $rules = ['name' => 'required'];
        $messages = ['name.required' => 'First name is required'];

        $this->validator->validate($data, $rules, $messages);
        $this->assertTrue($this->validator->passed());
        $this->assertEmpty($this->validator->getErrors());

        $data['name'] = '';
        $this->validator->reset()->validate($data, $rules, $messages);
        $this->assertTrue($this->validator->failed());
        $this->assertEquals('First name is required', $this->validator->getErrors()['name']['required']);
    }

    public function testValidateEquals()
    {
        $data = ['name' => 'John Doe'];
        $rules = ['name' => 'equals:John Snow'];

        $this->validator->validate($data, $rules);
        $this->assertTrue($this->validator->failed());
        $this->assertTrue($this->validator->hasError('name'));

        $data['name'] = 'John Snow';
        $this->validator->reset()->validate($data, $rules);
        $this->assertTrue($this->validator->passed());
        $this->assertFalse($this->validator->hasError('name'));
    }

    public function testValidateEqualsBooleanTrue()
    {
        $data = ['i_agree' => 1];
        $rules = ['i_agree' => 'equalsBooleanTrue'];
        $messages = ['i_agree.equalsBooleanTrue' => 'equals boolean true error'];

        $this->validator->validate($data, $rules, $messages);
        $this->assertFalse($this->validator->passed());
        $this->assertEquals($this->validator->getFirstError('i_agree'), 'equals boolean true error');


        $data['i_agree'] = true;
        $this->validator->reset()->validate($data, $rules);
        $this->assertTrue($this->validator->passed());
        $this->assertEquals($this->validator->getFirstError('i_agree'), '');
    }

    public function testValidateEqualsBooleanFalse()
    {
        $data = ['i_agree' => 0];
        $rules = ['i_agree' => 'equalsBooleanFalse'];

        $this->validator->validate($data, $rules);
        $this->assertFalse($this->validator->passed());

        $data['i_agree'] = false;
        $this->validator->reset()->validate($data, $rules);
        $this->assertTrue($this->validator->passed());
    }

    public function testValidateEmail()
    {
        $data = ['email_address' => 'adf'];
        $rules = ['email_address' => 'email'];
        $messages = ['email_address.email' => 'Invalid email'];

        $this->validator->validate($data, $rules, $messages);
        $this->assertTrue($this->validator->failed());
        $this->assertEquals('Invalid email', $this->validator->getErrors()['email_address']['email']);

        $data['email_address'] = 'valid_email@outlook.com';
        $this->validator->reset()->validate($data, $rules, $messages);
        $this->assertTrue($this->validator->passed());
        $this->assertEmpty($this->validator->getErrors());
    }

    public function testValidateInt()
    {
        $data = ['amount' => 'adf'];
        $rules = ['amount' => 'int'];
        $messages = ['amount.int' => 'Invalid integer.'];

        $this->validator->validate($data, $rules, $messages);
        $this->assertTrue($this->validator->failed());
        $this->assertEquals('Invalid integer.', $this->validator->getErrors()['amount']['int']);

        $data['amount'] = 100;
        $this->validator->reset()->validate($data, $rules, $messages);
        $this->assertTrue($this->validator->passed());
        $this->assertEmpty($this->validator->getErrors());

        // Float should fail too!
        $data['amount'] = 100.5;
        $this->validator->reset()->validate($data, $rules);
        $this->assertTrue($this->validator->failed());
    }

    public function testValidateUnsigned()
    {
        $data = ['amount' => -100];
        $rules = ['amount' => 'unsigned'];
        $messages = ['amount.unsigned' => 'Not a positive number.'];

        $this->validator->validate($data, $rules, $messages);
        $this->assertTrue($this->validator->failed());
        $this->assertEquals(
            'Not a positive number.',
            $this->validator->getErrors()['amount']['unsigned']
        );

        $data['amount'] = 12;
        $this->validator->reset()->validate($data, $rules, $messages);
        $this->assertTrue($this->validator->passed());
        $this->assertEmpty($this->validator->getErrors());
    }

    public function testValidateMin()
    {
        $data = ['num' => 'adf'];
        $rules = ['num' => 'min:17'];

        $this->validator->validate($data, $rules);
        $this->assertTrue($this->validator->failed());

        $data['num'] = 17.2;
        $this->validator->reset()->validate($data, $rules);
        $this->assertTrue($this->validator->passed());
        $this->assertEmpty($this->validator->getErrors());

        $data['num'] = 11.9;
        $this->validator->reset()->validate($data, $rules);
        $this->assertTrue($this->validator->failed());
    }

    public function testValidateMax()
    {
        $data = ['num' => 'adf'];
        $rules = ['num' => 'max:5'];

        $this->validator->validate($data, $rules);
        $this->assertTrue($this->validator->failed());

        $data['num'] = 5;
        $this->validator->reset()->validate($data, $rules);
        $this->assertTrue($this->validator->passed());
        $this->assertEmpty($this->validator->getErrors());

        $data['num'] = 5.1;
        $this->validator->reset()->validate($data, $rules);
        $this->assertTrue($this->validator->failed());
    }

    public function testValidateMinLength()
    {
        $data = ['string' => 'test'];
        $rules = ['string' => 'minLength:5'];

        $this->validator->validate($data, $rules);
        $this->assertTrue($this->validator->failed());

        $data['string'] = 'valid';
        $this->validator->reset()->validate($data, $rules);
        $this->assertTrue($this->validator->passed());
        $this->assertEmpty($this->validator->getErrors());
    }

    public function testValidateMaxLength()
    {
        $data = ['string' => 'test'];
        $rules = ['string' => 'maxLength:3'];

        $this->validator->validate($data, $rules);
        $this->assertTrue($this->validator->failed());

        $data['string'] = 'dog';
        $this->validator->reset()->validate($data, $rules);
        $this->assertTrue($this->validator->passed());
        $this->assertEmpty($this->validator->getErrors());
    }

    public function testValidateInCsv()
    {
        $csvLine = '5, 10, 20, 32, 53, 34';
        $data = ['input' => '100'];
        $rules = ['input' => 'inCsv:' . $csvLine];

        $this->validator->validate($data, $rules);
        $this->assertFalse($this->validator->passed());

        $data['input'] = 10;
        $this->validator->reset()->validate($data, $rules);
        $this->assertTrue($this->validator->passed());

        // string instead of csv elements, should fail
        $data['input'] = '10, 20';
        $this->validator->reset()->validate($data, $rules);
        $this->assertTrue($this->validator->failed());
    }
}
